﻿#include "LexAnaliz.h"
using namespace std;

int main()
{
	int z;
	string File;
	cout << "This is translator of the part of SIGNAL programming language" << endl << endl;
	cout << "Please, enter number of tests:" << endl << endl;
	std::cin >> z;
	for (int i = 1; i <= z; i++) {
		File = "test" + std::to_string(i);
		LeksAnaliz LexAn;
		LexAn.Analizator(File);
	}
	return 0;
}