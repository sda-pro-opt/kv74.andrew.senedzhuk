TRANSLATOR
В папці tests  будуть знаходитись папки з тестами. Для кожного тесту створено папку з його порядковим номером. В неї помісчено файл з кодом в форматі .sig, програма формує лістинг з такою ж назвою, тільки в форматі .lst в тому ж файлі. Для створення своїх тестів, потрібно створити папку test+порядковий номер на 1 більший, за наявний найбільший. в папці створити файл з розширенням .sig, в нього помістити вміст теста. При запуску програми потрібно ввести кількість тестів. Програма виведе результат Вашого тесту в ту ж папку з іншим розширенням.

GRAMATIC
Варіант 10

<signal-program> --> <program>
<program> --> PROGRAM <procedure-identifier> ; <block> ;
<block> --> <declarations> BEGIN <statementslist> END
<statements-list> --> <empty>
<declarations> --> <procedure-declarations>
<procedure-declarations> --> <procedure> <procedure-declarations> | <empty>
<procedure> --> PROCEDURE <procedureidentifier><parameters-list> ;
<parameters-list> --> ( <declarations-list> ) | <empty>
<declarations-list> --> <declaration> <declarations-list> | <empty>
<declaration> --><variableidentifier><identifierslist>:<attribute><attributes-list> ;
<identifiers-list> --> , <variable-identifier> <identifiers-list> | <empty>
<attributes-list> --> <attribute> <attributeslist> | <empty>
<attribute> --> SIGNAL | COMPLEX | INTEGER | FLOAT | BLOCKFLOAT | EXT
<variable-identifier> --> <identifier>
<procedure-identifier> --> <identifier>
<identifier> --> <letter><string>
<string> --> <letter><string> | <digit><string> | <empty>
<digit> --> 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 |
EXAMPLE TEXT CODE
PROGRAM TEST1;
PROCEDURE PROC1 (VAR1, VAR2 : INTEGER; VAR3: FLOAT);
PROCEDURE PROC2 ();
BEGIN
END.
EXAMPLE RESULT CODE
  Code      Line    Column                Name

   401         1         2             PROGRAM
  1001         1        10               TEST1
    59         1        15                   ;
   403         2         2           PROCEDURE
  1002         2        12               PROC1
    40         2        18                   (
  1003         2        19                VAR1
    44         2        23                   ,
  1004         2        25                VAR2
    58         2        30                   :
   406         2        32             INTEGER
    59         2        39                   ;
  1005         2        41                VAR3
    58         2        45                   :
   407         2        47               FLOAT
    41         2        52                   )
    59         2        53                   ;
   403         3         2           PROCEDURE
  1006         3        12               PROC2
    40         3        18                   (
    41         3        19                   )
    59         3        20                   ;
   403         4         2               BEGIN
   404         5         2                 END
    46         5         5                   .